package jullien.matheus.oiwarren.base.managers;

import org.junit.Test;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

/**
 * Created by Matheus Jullien on 01/10/2017.
 */
public class StringHelperTest {
    @Test
    public void isNullOrEmpty() throws Exception {
        assertTrue(StringManager.isNullOrEmpty(null));
        assertTrue(StringManager.isNullOrEmpty(""));
        assertFalse(StringManager.isNullOrEmpty("Teste"));
    }

    @Test
    public void isNotNullOrEmpty() throws Exception {
        assertFalse(StringManager.isNotNullOrEmpty(null));
        assertFalse(StringManager.isNotNullOrEmpty(""));
        assertTrue(StringManager.isNotNullOrEmpty("Teste"));
    }
}