package jullien.matheus.oiwarren.base.custom;

import android.content.Context;
import android.databinding.BindingAdapter;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

/**
 * Created by Matheus Jullien on 30/09/2017.
 */
public class TypeWriter extends AppCompatTextView {
    private CharSequence text;
    private int index;
    private long delay = 0;
    private Handler handler = new Handler();

    public TypeWriter(Context context) {
        super(context);
    }

    public TypeWriter(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    //TODO: refactor to animate text by phrase and set delay getting from text
    public void animateText(CharSequence text) {
        setText("");

        this.text = text;
        index = 0;

        handler.removeCallbacks(characterAdder);
        handler.postDelayed(characterAdder, delay);
    }

    public void setTextDelay(long delay) {
        this.delay = delay;
    }

    private Runnable characterAdder = new Runnable() {
        @Override
        public void run() {
            setText(text.subSequence(0, index++));

            if (index <= text.length()) {
                handler.postDelayed(characterAdder, delay);
            }
        }
    };

    @BindingAdapter(value = {"animateText", "animateDelay"}, requireAll = false)
    public static void animateText(TypeWriter typeWriter, String text, long delay) {
        typeWriter.animateText(text);
        typeWriter.setTextDelay(delay);
    }
}
