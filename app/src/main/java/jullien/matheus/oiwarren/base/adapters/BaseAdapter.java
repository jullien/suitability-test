package jullien.matheus.oiwarren.base.adapters;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.AbstractList;

/**
 * Created by Matheus Jullien on 01/10/2017.
 */
public abstract class BaseAdapter<T> extends RecyclerView.Adapter<BaseAdapter<T>.BaseViewHolder> {
    private AbstractList<T> list;

    public BaseAdapter(AbstractList<T> list) {
        this.list = list;
    }

    public AbstractList<T> getList() {
        return list;
    }

    public void setList(AbstractList<T> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public T getItem(int position) {
        return list.get(position);
    }

    public void addItem(T item) {
        this.list.add(item);
        notifyItemInserted(getItemCount());
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());

        ViewDataBinding binding = DataBindingUtil.inflate(layoutInflater, viewType, parent, false);

        return new BaseViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {}

    @Override
    public int getItemCount() {
        return list != null ? list.size() : 0;
    }

    public class BaseViewHolder extends RecyclerView.ViewHolder {
        private final ViewDataBinding binding;

        BaseViewHolder(ViewDataBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public ViewDataBinding getBinding() {
            return binding;
        }

        public void setBinding(Integer id, Object value) {
            binding.setVariable(id, value);
            binding.executePendingBindings();
        }
    }
}
