package jullien.matheus.oiwarren.base.managers;

/**
 * Created by Matheus Jullien on 01/10/2017.
 */
public class StringManager {
    //region --- VALIDATIONS ---
    public static boolean isNullOrEmpty(String s) {
        return s == null || s.isEmpty();
    }

    public static boolean isNotNullOrEmpty(String s) {
        return !isNullOrEmpty(s);
    }
    //endregion
}
