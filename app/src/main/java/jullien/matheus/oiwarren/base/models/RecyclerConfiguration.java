package jullien.matheus.oiwarren.base.models;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.BindingAdapter;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;

import jullien.matheus.oiwarren.BR;
import jullien.matheus.oiwarren.base.custom.SpacesItemDecoration;

/**
 * Created by Matheus Jullien on 30/09/2017.
 */
public class RecyclerConfiguration extends BaseObservable {
    private RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
    private RecyclerView.ItemDecoration itemDecoration = new SpacesItemDecoration(0);
    private RecyclerView.LayoutManager layoutManager;
    private RecyclerView.Adapter adapter;

    @Bindable
    public RecyclerView.LayoutManager getLayoutManager() {
        return layoutManager;
    }

    public void setLayoutManager(RecyclerView.LayoutManager layoutManager) {
        this.layoutManager = layoutManager;
        notifyPropertyChanged(BR.layoutManager);
    }

    @Bindable
    public RecyclerView.ItemAnimator getItemAnimator() {
        return itemAnimator;
    }

    public void setItemAnimator(RecyclerView.ItemAnimator itemAnimator) {
        this.itemAnimator = itemAnimator;
        notifyPropertyChanged(BR.itemAnimator);
    }

    @Bindable
    public RecyclerView.Adapter getAdapter() {
        return adapter;
    }

    public void setAdapter(RecyclerView.Adapter adapter) {
        this.adapter = adapter;
        notifyPropertyChanged(BR.adapter);
    }

    @Bindable
    public RecyclerView.ItemDecoration getItemDecoration() {
        return itemDecoration;
    }

    public void setItemDecoration(RecyclerView.ItemDecoration itemDecoration) {
        this.itemDecoration = itemDecoration;
        notifyPropertyChanged(BR.itemDecoration);
    }

    @BindingAdapter("configuration")
    public static void setRecyclerViewConfiguration(RecyclerView recyclerView, RecyclerConfiguration recyclerConfiguration) {
        recyclerView.addItemDecoration(recyclerConfiguration.getItemDecoration());
        recyclerView.setItemAnimator(recyclerConfiguration.getItemAnimator());
        recyclerView.setLayoutManager(recyclerConfiguration.getLayoutManager());
        recyclerView.setAdapter(recyclerConfiguration.getAdapter());
    }
}
