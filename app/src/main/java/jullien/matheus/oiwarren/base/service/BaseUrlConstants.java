package jullien.matheus.oiwarren.base.service;

/**
 * Created by Matheus Jullien on 30/09/2017.
 */
public class BaseUrlConstants {
    public static final String BASE_URL = "https://dev-api.oiwarren.com/";
}
