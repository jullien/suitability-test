package jullien.matheus.oiwarren.base.viewmodels;

import android.content.Context;
import android.databinding.BaseObservable;

/**
 * Created by Matheus Jullien on 30/09/2017.
 */
public class BaseViewModel extends BaseObservable {
    private Context context;

    public BaseViewModel(Context context) {
        this.context = context;
    }

    public Context getContext() {
        return this.context;
    }
}
