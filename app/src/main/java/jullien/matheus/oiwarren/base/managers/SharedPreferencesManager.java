package jullien.matheus.oiwarren.base.managers;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Matheus Jullien on 01/10/2017.
 */
public class SharedPreferencesManager {
    //region --- CONSTANTS ---
    private static final String KEY_USER_NAME_FIRST_LETTER = "userNameFirstLetter";
    //endregion

    //region --- VARIABLES ---
    private SharedPreferences prefs;
    //endregion

    //region --- CONSTRUCTORS ---
    public SharedPreferencesManager(Context context) {
        String PREFS_NAME = "userPreferences";

        prefs = context.getApplicationContext().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
    }
    //endregion

    //region --- GETTERS & SETTERS ---
    public String getUserName() {
        return prefs.getString(KEY_USER_NAME_FIRST_LETTER, "");
    }

    public void setUserName(String userName) {
        putString(KEY_USER_NAME_FIRST_LETTER, userName);
    }
    //endregion

    //region --- ACTIONS ---
    private void putString(String key, String value) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public void remove(String key) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.remove(key);
        editor.apply();
    }

    public void clear() {
        SharedPreferences.Editor editor = prefs.edit();
        editor.clear();
        editor.apply();
    }
    //endregion
}
