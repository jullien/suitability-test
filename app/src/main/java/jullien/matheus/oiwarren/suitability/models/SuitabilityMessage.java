package jullien.matheus.oiwarren.suitability.models;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import jullien.matheus.oiwarren.BR;

/**
 * Created by Matheus Jullien on 30/09/2017.
 */
public class SuitabilityMessage extends BaseObservable {
    private Integer layoutId = 0;
    private String type = "";
    private String value = "";

    public SuitabilityMessage(Integer layoutId, String type, String value) {
        this.layoutId = layoutId;
        this.type = type;
        this.value = value;
    }

    @Bindable
    public Integer getLayoutId() {
        return layoutId;
    }

    public void setLayoutId(Integer layoutId) {
        this.layoutId = layoutId;
        notifyPropertyChanged(BR.layoutId);
    }

    @Bindable
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
        notifyPropertyChanged(BR.type);
    }

    @Bindable
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
        notifyPropertyChanged(BR.value);
    }
}
