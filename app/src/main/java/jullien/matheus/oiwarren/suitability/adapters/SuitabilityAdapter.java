package jullien.matheus.oiwarren.suitability.adapters;

import android.view.ViewGroup;

import java.util.AbstractList;

import jullien.matheus.oiwarren.BR;
import jullien.matheus.oiwarren.R;
import jullien.matheus.oiwarren.base.adapters.BaseAdapter;
import jullien.matheus.oiwarren.base.managers.SharedPreferencesManager;
import jullien.matheus.oiwarren.suitability.models.SuitabilityMessage;

/**
 * Created by Matheus Jullien on 01/10/2017.
 */
public class SuitabilityAdapter extends BaseAdapter<SuitabilityMessage> {
    public SuitabilityAdapter(AbstractList<SuitabilityMessage> list) {
        super(list);
    }

    @Override
    public BaseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return super.onCreateViewHolder(parent, viewType);
    }

    @Override
    public void onBindViewHolder(BaseViewHolder holder, int position) {
        super.onBindViewHolder(holder, position);

        holder.setBinding(getId(getItemViewType(position)), getItem(position).getValue());

        if (isResponseId(getItemViewType(position))) {
            holder.setBinding(BR.firstLetter
                    , new SharedPreferencesManager(holder.itemView.getContext()).getUserName());
        }
    }

    @Override
    public int getItemViewType(int position) {
        return getList().get(position).getLayoutId();
    }

    private int getId(int itemViewType) {
        return itemViewType == R.layout.item_warren_message
                ? BR.message
                : BR.response;
    }

    private boolean isResponseId(int itemViewType) {
        return itemViewType == R.layout.item_user_response;
    }
}
