package jullien.matheus.oiwarren.suitability.models;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import jullien.matheus.oiwarren.BR;

/**
 * Created by Matheus Jullien on 30/09/2017.
 */
public class SuitabilityInput extends BaseObservable {
    private String type = "";
    private String mask = "";

    public SuitabilityInput(String type, String mask) {
        this.type = type;
        this.mask = mask;
    }

    @Bindable
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
        notifyPropertyChanged(BR.type);
    }

    @Bindable
    public String getMask() {
        return mask;
    }

    public void setMask(String mask) {
        this.mask = mask;
        notifyPropertyChanged(BR.mask);
    }
}
