package jullien.matheus.oiwarren.suitability.models;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.support.annotation.Nullable;

import java.util.HashMap;

import jullien.matheus.oiwarren.BR;

/**
 * Created by Matheus Jullien on 30/09/2017.
 */
public class SuitabilityRequest extends BaseObservable {
    private String context = "";
    private String id = "";
    private HashMap<String, Object> answers = new HashMap<>();

    public SuitabilityRequest(String context, @Nullable String id, HashMap<String, Object> answers) {
        this.context = context;
        this.id = id;
        this.answers = answers;
    }

    @Bindable
    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
        notifyPropertyChanged(BR.context);
    }

    @Bindable
    public String getId() {
        return id;
    }

    public void setId(@Nullable String id) {
        this.id = id;
        notifyPropertyChanged(BR.id);
    }

    @Bindable
    public HashMap<String, Object> getAnswers() {
        return answers;
    }

    public void setAnswers(HashMap<String, Object> answers) {
        this.answers = answers;
        notifyPropertyChanged(BR.answers);
    }

    public void putAnswer(String s, Object o) {
        this.answers.put(s, o);
        notifyPropertyChanged(BR.answers);
    }
}
