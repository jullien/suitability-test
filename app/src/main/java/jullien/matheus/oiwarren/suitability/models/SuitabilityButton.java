package jullien.matheus.oiwarren.suitability.models;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import jullien.matheus.oiwarren.BR;

/**
 * Created by Matheus Jullien on 30/09/2017.
 */
public class SuitabilityButton extends BaseObservable {
    private String value = "";
    private SuitabilityButtonLabel label = new SuitabilityButtonLabel("");

    public SuitabilityButton(String value, SuitabilityButtonLabel label) {
        this.value = value;
        this.label = label;
    }

    @Bindable
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
        notifyPropertyChanged(BR.value);
    }

    @Bindable
    public SuitabilityButtonLabel getLabel() {
        return label != null
                ? label
                : new SuitabilityButtonLabel("");
    }

    public void setLabel(SuitabilityButtonLabel label) {
        this.label = label;
        notifyPropertyChanged(BR.label);
    }
}
