package jullien.matheus.oiwarren.suitability.models;

import android.databinding.BaseObservable;
import android.databinding.Bindable;

import jullien.matheus.oiwarren.BR;

/**
 * Created by Matheus Jullien on 30/09/2017.
 */
public class SuitabilityButtonLabel extends BaseObservable {
    private String title = "";

    public SuitabilityButtonLabel(String title) {
        this.title = title;
    }

    @Bindable
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
        notifyPropertyChanged(BR.title);
    }
}
