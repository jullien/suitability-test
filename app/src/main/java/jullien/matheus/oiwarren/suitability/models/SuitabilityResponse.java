package jullien.matheus.oiwarren.suitability.models;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.support.annotation.Nullable;

import java.util.ArrayList;

import jullien.matheus.oiwarren.BR;

/**
 * Created by Matheus Jullien on 30/09/2017.
 */
public class SuitabilityResponse extends BaseObservable {
    private String id = "";
    private ArrayList<SuitabilityMessage> messages = new ArrayList<>();
    private ArrayList<SuitabilityInput> inputs = new ArrayList<>();
    private ArrayList<SuitabilityButton> buttons = new ArrayList<>();
    private ArrayList<String> responses = new ArrayList<>();

    public SuitabilityResponse(String id
            , ArrayList<SuitabilityMessage> messages
            , ArrayList<SuitabilityInput> inputs
            , ArrayList<SuitabilityButton> buttons
            , ArrayList<String> responses) {
        this.id = id;
        this.messages = messages;
        this.inputs = inputs;
        this.buttons = buttons;
        this.responses = responses;
    }

    @Bindable
    public String getId() {
        return id;
    }

    public void setId(@Nullable String id) {
        this.id = id;
        notifyPropertyChanged(BR.id);
    }

    @Bindable
    public ArrayList<SuitabilityMessage> getMessages() {
        return messages != null
                ? messages
                : new ArrayList<SuitabilityMessage>();
    }

    public void setMessages(ArrayList<SuitabilityMessage> messages) {
        this.messages = messages;
        notifyPropertyChanged(BR.messages);
    }

    @Bindable
    public ArrayList<SuitabilityInput> getInputs() {
        return inputs != null
                ? inputs
                : new ArrayList<SuitabilityInput>();
    }

    public void setInputs(ArrayList<SuitabilityInput> inputs) {
        this.inputs = inputs;
        notifyPropertyChanged(BR.inputs);
    }

    @Bindable
    public ArrayList<SuitabilityButton> getButtons() {
        return buttons != null
                ? buttons
                : new ArrayList<SuitabilityButton>();
    }

    public void setButtons(ArrayList<SuitabilityButton> buttons) {
        this.buttons = buttons;
        notifyPropertyChanged(BR.buttons);
    }

    @Bindable
    public ArrayList<String> getResponses() {
        return responses != null
                ? responses
                : new ArrayList<String>();
    }

    public void setResponses(ArrayList<String> responses) {
        this.responses = responses;
        notifyPropertyChanged(BR.responses);
    }
}
