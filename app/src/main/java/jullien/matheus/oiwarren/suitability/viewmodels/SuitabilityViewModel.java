package jullien.matheus.oiwarren.suitability.viewmodels;

import android.content.Context;
import android.databinding.Bindable;
import android.databinding.ObservableField;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import jullien.matheus.oiwarren.BR;
import jullien.matheus.oiwarren.R;
import jullien.matheus.oiwarren.base.managers.SharedPreferencesManager;
import jullien.matheus.oiwarren.base.managers.StringManager;
import jullien.matheus.oiwarren.base.models.RecyclerConfiguration;
import jullien.matheus.oiwarren.base.viewmodels.BaseViewModel;
import jullien.matheus.oiwarren.suitability.adapters.SuitabilityAdapter;
import jullien.matheus.oiwarren.suitability.models.SuitabilityButton;
import jullien.matheus.oiwarren.suitability.models.SuitabilityMessage;
import jullien.matheus.oiwarren.suitability.models.SuitabilityRequest;
import jullien.matheus.oiwarren.suitability.models.SuitabilityResponse;
import jullien.matheus.oiwarren.suitability.services.SuitabilityServiceViewModel;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Matheus Jullien on 30/09/2017.
 */
public class SuitabilityViewModel extends BaseViewModel {
    //region --- CONSTANTS ---
    private static final String SUITABILITY = "suitability";
    private static final String ID_QUESTION_NAME = "question_name";
    private static final String ID_FINAL = "final";
    public static final String PASSIVE = "P";
    public static final String ACTIVE = "A";
    //endregion

    //region --- VARIABLES ---
    private SuitabilityRequest suitabilityRequest;

    private SuitabilityAdapter suitabilityAdapter;
    private RecyclerConfiguration recyclerConfiguration;

    private ObservableField<Boolean> isResponseVisible = new ObservableField<>(false);
    private ObservableField<Boolean> isResponseInput = new ObservableField<>(false);
    private ObservableField<Boolean> isResponseButtons = new ObservableField<>(false);
    private ObservableField<Boolean> isResponseSeeProfile = new ObservableField<>(false);

    private ObservableField<String> response = new ObservableField<>("");
    private ObservableField<String> responseHint = new ObservableField<>("");
    private ObservableField<String> passiveLabel = new ObservableField<>("");
    private ObservableField<String> activeLabel = new ObservableField<>("");
    //endregion

    //region --- CONSTRUCTORS ---
    public SuitabilityViewModel(Context context) {
        super(context);

        setSuitabilityRequest(new SuitabilityRequest(SUITABILITY
                , null
                , new HashMap<String, Object>()));

        setSuitabilityAdapter(new SuitabilityAdapter(new ArrayList<SuitabilityMessage>()));
        setRecyclerConfiguration(new RecyclerConfiguration());

        postConversationMessage(getSuitabilityRequest());
    }
    //endregion

    //region --- GETTERS & SETTERS ---
    @Bindable
    public SuitabilityRequest getSuitabilityRequest() {
        return suitabilityRequest;
    }

    public void setSuitabilityRequest(SuitabilityRequest suitabilityRequest) {
        this.suitabilityRequest = suitabilityRequest;
        notifyPropertyChanged(BR.suitabilityRequest);
    }

    @Bindable
    public SuitabilityAdapter getSuitabilityAdapter() {
        return suitabilityAdapter;
    }

    public void setSuitabilityAdapter(SuitabilityAdapter suitabilityAdapter) {
        this.suitabilityAdapter = suitabilityAdapter;
        notifyPropertyChanged(BR.suitabilityAdapter);
    }

    @Bindable
    public RecyclerConfiguration getRecyclerConfiguration() {
        return recyclerConfiguration;
    }

    public void setRecyclerConfiguration(RecyclerConfiguration recyclerConfiguration) {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        linearLayoutManager.setAutoMeasureEnabled(true);

        this.recyclerConfiguration = recyclerConfiguration;
        this.recyclerConfiguration.setLayoutManager(linearLayoutManager);
        this.recyclerConfiguration.setAdapter(getSuitabilityAdapter());

        notifyPropertyChanged(BR.recyclerConfiguration);
    }

    public ObservableField<Boolean> getIsResponseVisible() {
        return isResponseVisible;
    }

    public void setIsResponseVisible(Boolean isResponseVisible) {
        this.isResponseVisible.set(isResponseVisible);
    }

    public ObservableField<Boolean> getIsResponseInput() {
        return isResponseInput;
    }

    public void setIsResponseInput(Boolean isResponseInput) {
        this.isResponseInput.set(isResponseInput);
    }

    public ObservableField<Boolean> getIsResponseButtons() {
        return isResponseButtons;
    }

    public void setIsResponseButtons(Boolean isResponseButtons) {
        this.isResponseButtons.set(isResponseButtons);
    }

    public ObservableField<Boolean> getIsResponseSeeProfile() {
        return isResponseSeeProfile;
    }

    public void setIsResponseSeeProfile(Boolean isResponseSeeProfile) {
        this.isResponseSeeProfile.set(isResponseSeeProfile);
    }

    public ObservableField<String> getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response.set(response);
    }

    public ObservableField<String> getResponseHint() {
        return responseHint;
    }

    public void setResponseHint(String responseHint) {
        this.responseHint.set(responseHint);
    }

    public ObservableField<String> getPassiveLabel() {
        return passiveLabel;
    }

    public void setPassiveLabel(String passiveLabel) {
        this.passiveLabel.set(passiveLabel);
    }

    public ObservableField<String> getActiveLabel() {
        return activeLabel;
    }

    public void setActiveLabel(String activeLabel) {
        this.activeLabel.set(activeLabel);
    }
    //endregion

    //region --- REQUESTS ---
    private void postConversationMessage(SuitabilityRequest request) {
        new SuitabilityServiceViewModel()
                .postConversationMessage(request)
                .enqueue(new Callback<SuitabilityResponse>() {
                    @Override
                    public void onResponse(@NonNull Call<SuitabilityResponse> call
                            , @NonNull Response<SuitabilityResponse> response) {
                        if (response.isSuccessful() && response.body() != null) {
                            SuitabilityResponse suitabilityResponse = response.body();

                            getSuitabilityRequest().setId(suitabilityResponse.getId());
                            addMessagesToAdapter(suitabilityResponse.getMessages());
                            showResponseByType(suitabilityResponse.getId(), suitabilityResponse.getButtons());

                            if (!suitabilityResponse.getResponses().isEmpty()) {
                                setResponseHint(suitabilityResponse
                                        .getResponses()
                                        .get(0)
                                        .split("\\{")[0]
                                        .trim());
                            } else {
                                setResponseHint("");
                            }
                        } else {
                            Log.e("Error", response.message());
                        }

                        setIsResponseVisible(true);
                        scrollToBottom(getRecyclerConfiguration());
                    }

                    @Override
                    public void onFailure(@NonNull Call<SuitabilityResponse> call
                            , @NonNull Throwable t) {
                        Log.e("Error", t.getLocalizedMessage());

                        setIsResponseVisible(true);
                    }
                });
    }

    private void postSuitabilityFinish(HashMap<String, Object> answers) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("answers", new Gson().toJson(answers));

        new SuitabilityServiceViewModel().postSuitabilityFinish(jsonObject).enqueue(new Callback<JSONObject>() {
            @Override
            public void onResponse(@NonNull Call<JSONObject> call
                    , @NonNull Response<JSONObject> response) {
                if (response.isSuccessful() && response.body() != null) {
                    //TODO: parse response to show user profile
                } else {
                    Log.e("Error", response.message());

                    setIsResponseVisible(true);
                }
            }

            @Override
            public void onFailure(@NonNull Call<JSONObject> call, @NonNull Throwable t) {
                Log.e("Error", t.getLocalizedMessage());

                setIsResponseVisible(true);
            }
        });
    }
    //endregion

    //region --- LISTENERS ---
    public View.OnClickListener onResponseClick(final String response) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (StringManager.isNotNullOrEmpty(response)) {
                    setIsResponseVisible(false);

                    if (getSuitabilityRequest().getId().equalsIgnoreCase(ID_QUESTION_NAME)) {
                        new SharedPreferencesManager(getContext()).setUserName(response.substring(0,1));
                    }

                    getSuitabilityAdapter().addItem(createSuitabilityMessageByResponse(response));
                    getSuitabilityRequest().putAnswer(getSuitabilityRequest().getId(), response);

                    postConversationMessage(getSuitabilityRequest());
                }
            }
        };
    }

    public View.OnClickListener onSeeProfileClick() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setIsResponseVisible(false);

                postSuitabilityFinish(getSuitabilityRequest().getAnswers());
            }
        };
    }
    //endregion

    //region --- ACTIONS ---
    //TODO: add next message only after the previous one appears completely
    private void addMessagesToAdapter(ArrayList<SuitabilityMessage> messages) {
        for (int i = 0; i < messages.size(); i++) {
            messages.get(i).setLayoutId(R.layout.item_warren_message);

            getSuitabilityAdapter().addItem(messages.get(i));
        }
    }

    private void showResponseByType(String id, ArrayList<SuitabilityButton> buttons) {
        if (!id.equals(ID_FINAL)) {
            setIsResponseSeeProfile(false);

            if (buttons.isEmpty()) {
                setIsResponseButtons(false);
                setIsResponseInput(true);

                setResponse("");
            } else {
                setIsResponseInput(false);
                setIsResponseButtons(true);

                SuitabilityButton passiveButton = buttons.get(0);
                SuitabilityButton activeButton = buttons.get(1);

                setPassiveLabel(passiveButton.getLabel().getTitle());
                setActiveLabel(activeButton.getLabel().getTitle());
            }
        } else {
            setIsResponseInput(false);
            setIsResponseButtons(false);
            setIsResponseSeeProfile(true);
        }
    }

    private SuitabilityMessage createSuitabilityMessageByResponse(String response) {
        switch (response) {
            case PASSIVE:
                return new SuitabilityMessage(R.layout.item_user_response
                        , "string"
                        , getPassiveLabel().get());
            case ACTIVE:
                return new SuitabilityMessage(R.layout.item_user_response
                        , "string"
                        , getActiveLabel().get());
            default:
                //TODO: use input response to format response
                return new SuitabilityMessage(R.layout.item_user_response, "string", response);
        }
    }

    private void scrollToBottom(RecyclerConfiguration recyclerConfiguration) {
        recyclerConfiguration.getLayoutManager()
                .scrollToPosition(recyclerConfiguration.getAdapter().getItemCount() - 1);
    }
    //endregion
}
