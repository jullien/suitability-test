package jullien.matheus.oiwarren.suitability.views;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import jullien.matheus.oiwarren.R;
import jullien.matheus.oiwarren.base.managers.SharedPreferencesManager;
import jullien.matheus.oiwarren.databinding.ActivitySuitabilityBinding;
import jullien.matheus.oiwarren.suitability.viewmodels.SuitabilityViewModel;

public class SuitabilityActivity extends AppCompatActivity {
    private SuitabilityViewModel viewModel;

    private ActivitySuitabilityBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        super.onResume();

        setupViewModel();
        setupActivityBinding();
        setupViewModelBinding();
    }

    private void setupViewModel() {
        viewModel = new SuitabilityViewModel(this);
    }

    private void setupActivityBinding() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_suitability);
    }

    private void setupViewModelBinding() {
        binding.setViewModel(viewModel);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        new SharedPreferencesManager(this).clear();
    }
}
