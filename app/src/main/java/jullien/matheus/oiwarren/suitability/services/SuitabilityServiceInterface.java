package jullien.matheus.oiwarren.suitability.services;

import com.google.gson.JsonObject;

import org.json.JSONObject;

import jullien.matheus.oiwarren.suitability.models.SuitabilityRequest;
import jullien.matheus.oiwarren.suitability.models.SuitabilityResponse;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

/**
 * Created by Matheus Jullien on 30/09/2017.
 */
public interface SuitabilityServiceInterface {
    @POST("api/v2/conversation/message")
    Call<SuitabilityResponse> postConversationMessage(@Body SuitabilityRequest suitabilityRequest);

    @POST("api/v2/suitability/finish")
    Call<JSONObject> postSuitabilityFinish(@Body JsonObject jsonObject);
}
