package jullien.matheus.oiwarren.suitability.services;

import com.google.gson.JsonObject;

import org.json.JSONObject;

import jullien.matheus.oiwarren.base.service.BaseUrlConstants;
import jullien.matheus.oiwarren.suitability.models.SuitabilityRequest;
import jullien.matheus.oiwarren.suitability.models.SuitabilityResponse;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Matheus Jullien on 30/09/2017.
 */
public class SuitabilityServiceViewModel {
    private SuitabilityServiceInterface suitabilityServiceInterface;

    public SuitabilityServiceViewModel() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BaseUrlConstants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        suitabilityServiceInterface = retrofit.create(SuitabilityServiceInterface.class);
    }

    public Call<SuitabilityResponse> postConversationMessage(SuitabilityRequest suitabilityRequest) {
        return suitabilityServiceInterface.postConversationMessage(suitabilityRequest);
    }

    public Call<JSONObject> postSuitabilityFinish(JsonObject jsonObject) {
        return suitabilityServiceInterface.postSuitabilityFinish(jsonObject);
    }
}
